'use strict'

const chalk = require('chalk')
const email = require('./mandrill-munily')

let mailData = {
  to: 'mitchellcontreras@gmail.com',
  subject: 'Correo de munily',
  template: 'myTemplate.html',
  param: {
    title: 'Titulo del correo',
    h1: 'Titulo del mensaje 12',
    body: 'El mensaje del correo es'
  }
}

email.sendTemplate(mailData)
  .then(response => console.log(`${chalk.green('[Sent]')} ${JSON.stringify(response)}`))
  .catch(err => console.log(`${chalk.red('[Error]')} ${JSON.stringify(err)}`))
