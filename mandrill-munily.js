'use strict'

const config = require('./config')
const fs = require('fs')
const handlebars = require('handlebars')
const mandrillTransport = require('nodemailer-mandrill-transport')
const nodemailer = require('nodemailer')
const util = require('util')

const readFile = util.promisify(fs.readFile)
let transportInstance = null

if (!transportInstance) {
  transportInstance = nodemailer.createTransport(mandrillTransport({
    auth: {
      apiKey: config.mandrill.apiKey
    }
  }))
}

function createRouteTemplate (template) {
  return config.dirTemplate + template
}

function sendTemplate ({ to, subject, template, param }) {
  return new Promise(async (resolve, reject) => {
    const routeTemplate = createRouteTemplate(template)

    let mailData = {
      from: config.mandrill.from,
      to: to,
      subject: subject,
      html: handlebars.compile(await readFile(routeTemplate, 'utf8'))(param)
    }

    transportInstance.sendMail(mailData, (err, response) => {
      if (err) {
        reject(err)
      }
      resolve(response)
    })
  })
}

module.exports = {
  sendTemplate
}
